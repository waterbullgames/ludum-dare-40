﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    public static LevelManager instance;
    public int numberOfDrinks = 3;
    public int numDrinksUntilBlackout = 10;
    public Transform[] glassLocations;
    public GlassToGlassPrefab[] glassPrefabs;
    public DecorationToDecorationPrefab[] decorationPrefabs;
    public IntoxicationManager intoxManager;
    public Text scoreText;
    public GameObject drinkButtons;

    IAlcoholExpert alcoholExpert;
    Dictionary<Glass, GameObject> glassToPrefabsMap;
    Dictionary<Decoration, GameObject> decorationToDecorationPrefabMap;
    GameObject[] drinkObjects;
    float drinksDrank = 0;

    int alcoholLevel;

    [SerializeField]
    DrinkProperties[] drinks;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            drinkObjects = new GameObject[numberOfDrinks];          
            glassToPrefabsMap = new Dictionary<Glass, GameObject>();
            decorationToDecorationPrefabMap = new Dictionary<Decoration, GameObject>();

            foreach (GlassToGlassPrefab glassPrefab in glassPrefabs)
            {
                glassToPrefabsMap[glassPrefab.glass] = glassPrefab.prefab;
            }
            foreach (DecorationToDecorationPrefab decorationToDecorationPrefab in decorationPrefabs)
            {
                decorationToDecorationPrefabMap[decorationToDecorationPrefab.decoration] = decorationToDecorationPrefab.prefab;
            } 
        }
        else
            Destroy(this);
    }

    // Use this for initialization
    void Start ()
    {
        ResetLevel();
    }

    private void ServeDrinks()
    {
        drinks = alcoholExpert.GetDrinks(3, 1);

        for (int i = 0; i < drinks.Length; i++)
        {
            DrinkProperties drinkToServe = drinks[i];
            GameObject drinkPrefab = glassToPrefabsMap[drinkToServe.glass];
            GameObject decorationPrefab = decorationToDecorationPrefabMap[drinkToServe.decoration];
            GameObject drinkObject = Instantiate(drinkPrefab, glassLocations[i].position, Quaternion.identity);
            GameObject decorationObject = Instantiate(decorationPrefab); 

            drinkObject.name = "Drink" + i;
            Drink drink = drinkObject.GetComponent<Drink>();
            drink.SetColor(drinkToServe.drinkColor);
            drink.SetDecoration(decorationObject);
            
            drinkObjects[i] = drinkObject;
        }
    }

    public void RemoveDrinks()
    {
        foreach (var drinkObject in drinkObjects)
        {
            Destroy(drinkObject);
        }
    }

    public void ChooseDrink(int drinkNumber)
    {
        Debug.Log(drinkNumber);
        DrinkProperties chosenDrink = drinks[drinkNumber-1];
        bool containsAlcohol = alcoholExpert.ContainsAlcohol(chosenDrink);

        if (containsAlcohol)
        {
            Debug.Log("Alcohol!");
            alcoholLevel++;
            float intoxicationPercentage = (float)alcoholLevel / numDrinksUntilBlackout;
            intoxManager.SetIntoxicationPercentage(intoxicationPercentage);
        }
        else
        {
            Debug.Log("Normal drink!");
        }

        drinksDrank++;
        scoreText.text = "Drinks: " + drinksDrank;
        RemoveDrinks();

        if (intoxManager.ReadyForBlackout())
        {
            drinkButtons.SetActive(false);
            intoxManager.Blackout(BlackoutFinished);
        }else
            ServeDrinks();
    }

    public void BlackoutFinished()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ResetLevel();
    }

    private void ResetLevel()
    {
        alcoholLevel = 0;
        IAlcoholExpert[] possibleExperts = new IAlcoholExpert[] { new GlassAlcoholExpert(), new ColorAlcoholExpert(), new DecorationAlcoholExpert() };
        alcoholExpert = possibleExperts[UnityEngine.Random.Range(0, possibleExperts.Length)];

        Debug.Log(alcoholExpert);
        intoxManager.ResetLevel();
        drinkButtons.SetActive(true);
        drinksDrank = 0;
        scoreText.text = "Drinks: " + drinksDrank;

        ServeDrinks();
    }

    public int GetNumPossibleValuesPerElement()
    {
        return 3;
    }

    [Serializable]
    public struct GlassToGlassPrefab
    {
        public Glass glass;
        public GameObject prefab;
    }

    [Serializable]
    public struct DecorationToDecorationPrefab
    {
        public Decoration decoration;
        public GameObject prefab;
    }
}

