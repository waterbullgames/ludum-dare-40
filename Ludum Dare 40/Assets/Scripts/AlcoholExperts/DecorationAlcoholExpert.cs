﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorationAlcoholExpert : IAlcoholExpert {

    public Decoration decorationWithAlcohol;

    public DecorationAlcoholExpert(Decoration decoration)
    {
        decorationWithAlcohol = decoration;
    }

    public override string ToString()
    {
        return "Alcohol decoration is: " + decorationWithAlcohol.ToString();
    }

    public DecorationAlcoholExpert(): this(DrinkProperties.GetRandomDecoration()){ }

    public override bool ContainsAlcohol(DrinkProperties drinkProps)
    {
        return drinkProps.decoration == decorationWithAlcohol;
    }

    public override Decoration GetNewDecorationWithAlcohol()
    {
        return decorationWithAlcohol;
    }

}
