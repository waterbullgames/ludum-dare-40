﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ColorAlcoholExpert : IAlcoholExpert
{
    public DrinkColor drinkColorWithAlcohol;

    public ColorAlcoholExpert( DrinkColor drinkColor )
    {
        drinkColorWithAlcohol = drinkColor;
    }

    public override string ToString()
    {
        return "Alcohol color is: " + drinkColorWithAlcohol.ToString();
    }

    public ColorAlcoholExpert(): this(DrinkProperties.GetRandomDrinkColor()){}

    public override bool ContainsAlcohol(DrinkProperties drinkProps)
    {
        return drinkProps.drinkColor == drinkColorWithAlcohol;
    }

    public override DrinkColor GetNewDrinkColorWithAlcohol()
    {
        return drinkColorWithAlcohol;
    }

}
