﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GlassAlcoholExpert : IAlcoholExpert {

    public Glass glassWithAlcohol;

    public GlassAlcoholExpert(Glass glass)
    {
        glassWithAlcohol = glass;
    }

    public override string ToString()
    {
        return "Alcohol glass is: " + glassWithAlcohol.ToString();
    }

    public GlassAlcoholExpert(): this(DrinkProperties.GetRandomGlass()){ }

    public override bool ContainsAlcohol(DrinkProperties drinkProps)
    {
        return drinkProps.glass == glassWithAlcohol;
    }

    public override Glass GetNewGlassWithAlcohol()
    {
        return glassWithAlcohol;
    }

}
