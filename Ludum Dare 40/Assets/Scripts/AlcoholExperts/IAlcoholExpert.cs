﻿
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class IAlcoholExpert {


    public abstract bool ContainsAlcohol(DrinkProperties drinkProps);


    public DrinkProperties[] GetDrinks(int amount = 3, int withAlcohol = 1)
    {
        HashSet<DrinkProperties> drinksToServe = new HashSet<DrinkProperties>();

        // Create drinks with alcohol
        while (drinksToServe.Count < withAlcohol)
        {
            DrinkProperties drinkProps = new DrinkProperties();
            drinkProps.glass = GetNewGlassWithAlcohol();
            drinkProps.drinkColor = GetNewDrinkColorWithAlcohol();
            drinkProps.decoration = GetNewDecorationWithAlcohol();

            if (!drinksToServe.Contains(drinkProps))
                drinksToServe.Add(drinkProps);
        }

        //Generate drinks without alcohol
        while (drinksToServe.Count < amount)
        {
            DrinkProperties drinkProps = new DrinkProperties();
            drinkProps.glass = DrinkProperties.GetRandomGlass();
            do
            {
                drinkProps.drinkColor = DrinkProperties.GetRandomDrinkColor();
            } while (ContainsAlcohol(drinkProps));

            drinkProps.decoration = DrinkProperties.GetRandomDecoration();

            if (!drinksToServe.Contains(drinkProps))
                drinksToServe.Add(drinkProps);
        }

        //Shuffle the drinks before returning
        return drinksToServe.OrderBy(x => Random.Range(0, 1000)).ToArray();
    }

   

    public virtual Glass GetNewGlassWithAlcohol()
    {
        return DrinkProperties.GetRandomGlass();
    }

    public virtual DrinkColor GetNewDrinkColorWithAlcohol()
    {
        return DrinkProperties.GetRandomDrinkColor();
    }

    public virtual Decoration GetNewDecorationWithAlcohol()
    {
        return DrinkProperties.GetRandomDecoration();
    }
}
