﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.PostProcessing;
using UnityEngine;
using UnityEngine.UI;
using System;

public class IntoxicationManager : MonoBehaviour {

    public PostProcessingProfile postProcessProfile;
    public Slider intoxicationMeter;
    public float startValue = 125;
    public float maxValue = 300;
    public float blackoutVignetteDuration = 0;
    public float blackoutBackgroundFadeDuration = 0;
    public SpriteRenderer blackoutImage;

    float intoxicationPercentage = 0;

    void Start()
    {
        SetFocalLength(startValue);
        intoxicationMeter.value = 0;
        SetVignetteIntensity(0);
        SetBlackoutAlpha(0);
    }

    public void SetIntoxicationPercentage(float intoxicationPercentage)
    {
        this.intoxicationPercentage = intoxicationPercentage;
        Debug.Log("IntoxicationPerc " + intoxicationPercentage);
        float newFocalLength = startValue + (maxValue - startValue) * intoxicationPercentage;
        Debug.Log("newFocalLength " + newFocalLength);
        SetFocalLength(newFocalLength);
        intoxicationMeter.value = intoxicationPercentage;
    }

    public bool ReadyForBlackout()
    {
        return intoxicationPercentage >= 1;
    }

    private void SetFocalLength(float focalLength)
    {
        DepthOfFieldModel.Settings tmp = postProcessProfile.depthOfField.settings;
        tmp.focalLength = focalLength;
        postProcessProfile.depthOfField.settings = tmp;
    }

    private void SetVignetteIntensity(float intensity)
    {
        var vignetteSettings = postProcessProfile.vignette.settings;
        vignetteSettings.intensity = intensity;
        postProcessProfile.vignette.settings = vignetteSettings;
    }

    public void Blackout(Action blackoutFinished)
    {
        StartCoroutine(BlackoutCoroutine(blackoutFinished));
    }

    private IEnumerator BlackoutCoroutine(Action blackoutFinished)
    {
        float blackoutVignetteTime = 0;
        while (blackoutVignetteTime <= blackoutVignetteDuration)
        {
            float newIntensity = blackoutVignetteTime / blackoutVignetteDuration;
            SetVignetteIntensity(newIntensity);
            yield return null;
            blackoutVignetteTime += Time.deltaTime;
        }

        float blackoutBackgroundFadeTime = 0;
        while (blackoutBackgroundFadeTime <= blackoutBackgroundFadeDuration)
        {
            float newAlphaPercent = blackoutBackgroundFadeTime / blackoutBackgroundFadeDuration;
            SetBlackoutAlpha(newAlphaPercent);
            yield return null;
            blackoutBackgroundFadeTime += Time.deltaTime;
        }

        blackoutFinished();
    }

    private void SetBlackoutAlpha(float newAlphaPercent)
    {
        Color color = blackoutImage.color;
        color.a = newAlphaPercent;
        blackoutImage.color = color;
    }

    void OnApplicationQuit()
    {
        SetFocalLength(0);
        SetVignetteIntensity(0);
        SetBlackoutAlpha(0);
    }

    internal void ResetLevel()
    {
        SetBlackoutAlpha(0);
        SetVignetteIntensity(0);
        SetIntoxicationPercentage(0);
    }
}
