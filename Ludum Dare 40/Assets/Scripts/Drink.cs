﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drink : MonoBehaviour {
  
    public Transform decorationLocation;
    public Transform liquidLocation;
    public DrinkColorToColorPrefab[] drinkColorToColorPrefabList;

    Dictionary<DrinkColor, GameObject> DrinkColorToColoPrefabMap;

    void Awake ()
    {
        DrinkColorToColoPrefabMap = new Dictionary<DrinkColor, GameObject>();

        foreach (DrinkColorToColorPrefab drinkColorToColorPrefab in drinkColorToColorPrefabList)
        {
            DrinkColorToColoPrefabMap[drinkColorToColorPrefab.drinkColor] = drinkColorToColorPrefab.prefab;
        }

    }

    public void SetColor(DrinkColor drinkColor)
    {
        GameObject drinkColorPrefab = DrinkColorToColoPrefabMap[drinkColor];
        GameObject drinkColorObject = Instantiate(drinkColorPrefab);

        drinkColorObject.transform.SetParent(this.transform);
        drinkColorObject.transform.localPosition = liquidLocation.localPosition;
    }

    public void SetDecoration(GameObject decoration)
    {
        decoration.transform.SetParent(this.transform);
        decoration.transform.localPosition = decorationLocation.localPosition;
    }

}

[Serializable]
public struct DrinkColorToColorPrefab
{
    public DrinkColor drinkColor;
    public GameObject prefab;
}