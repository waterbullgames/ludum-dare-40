﻿using System;
using UnityEngine;

[Serializable]
public class DrinkProperties {

    public Glass glass;
    public DrinkColor drinkColor;
    public Decoration decoration;

    public DrinkProperties() {}

    public DrinkProperties(Glass glass, DrinkColor drinkColor, Decoration decoration)
    {
        this.glass = glass;
        this.drinkColor = drinkColor;
        this.decoration = decoration;
    }

    public override bool Equals(object obj)
    {
        var otherDrinkProps = obj as DrinkProperties;

        if (otherDrinkProps == null)
        {
            return false;
        }

        return glass == otherDrinkProps.glass && 
            drinkColor == otherDrinkProps.drinkColor &&
            decoration == otherDrinkProps.decoration;
    }

    public override int GetHashCode()
    {
        return ("" + glass + drinkColor + decoration).GetHashCode();
    }

    public static Glass GetRandomGlass()
    {
        return (Glass)GetRandomValueFromEnum<Glass>();
    }

    public static DrinkColor GetRandomDrinkColor()
    {
        return (DrinkColor)GetRandomValueFromEnum<DrinkColor>();
    }

    public static Decoration GetRandomDecoration()
    {
        return (Decoration)GetRandomValueFromEnum<Decoration>();
    }

    private static int GetRandomValueFromEnum<T>() where T: IConvertible
    {
        int maxValueEnum =Enum.GetNames(typeof(T)).Length;
        int maxValueSetting = LevelManager.instance.GetNumPossibleValuesPerElement();
        int maxValue = Mathf.Min(maxValueEnum, maxValueSetting);
      
        int intValue = UnityEngine.Random.Range(0, maxValue);
        return intValue;
    }
}
public enum Glass { Martini = 0, Cocktail = 1, Margarita = 2};
public enum DrinkColor { Blue = 0, Green = 1, Red = 2 };
public enum Decoration { Olive = 0, OrangeSlice = 1, Straw = 2 };